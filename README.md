The Nossa Law Office is a client oriented firm, meaning the client or consult comes first. Honesty and straightforwardness are both qualities that we express in each and every interaction. We never take cases unless we believe that there is a form of relief available and that we can succeed.

Address: 1415 North Loop West, Suite 905, Houston, TX 77008, USA

Phone: 713-599-1633